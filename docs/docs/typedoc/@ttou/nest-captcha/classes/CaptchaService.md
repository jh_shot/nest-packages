[Documentation](../../../index.md) / [@ttou/nest-captcha](../index.md) / CaptchaService

# Class: CaptchaService

## Constructors

### new CaptchaService(cacheManager, options)

> **new CaptchaService**(`cacheManager`, `options`): [`CaptchaService`](CaptchaService.md)

#### Parameters

• **cacheManager**: `Cache`

• **options**: [`CaptchaOptions`](../interfaces/CaptchaOptions.md)

#### Returns

[`CaptchaService`](CaptchaService.md)

#### Source

captcha.service.ts:18

## Properties

### cacheManager

> **`private`** **cacheManager**: `Cache`

#### Source

captcha.service.ts:20

***

### options

> **`private`** **options**: [`CaptchaOptions`](../interfaces/CaptchaOptions.md)

#### Source

captcha.service.ts:22

## Methods

### check()

> **check**(`id`, `value`): `Promise`\<`boolean`\>

#### Parameters

• **id**: `string`

• **value**: `string`

#### Returns

`Promise`\<`boolean`\>

#### Source

captcha.service.ts:116

***

### formula()

> **formula**(`options`?): `Promise`\<`Object`\>

#### Parameters

• **options?**: `FormulaCaptchaOptions`

#### Returns

`Promise`\<`Object`\>

> ##### id
>
> > **id**: `string`
>
> ##### imageBase64
>
> > **imageBase64**: `string`
>

#### Source

captcha.service.ts:57

***

### getStoreId()

> **`private`** **getStoreId**(`id`): `string`

#### Parameters

• **id**: `string`

#### Returns

`string`

#### Source

captcha.service.ts:129

***

### image()

> **image**(`options`?): `Promise`\<`Object`\>

#### Parameters

• **options?**: `ImageCaptchaOptions`

#### Returns

`Promise`\<`Object`\>

> ##### id
>
> > **id**: `string`
>
> ##### imageBase64
>
> > **imageBase64**: `string`
>

#### Source

captcha.service.ts:25

***

### set()

> **set**(`text`): `Promise`\<`string`\>

#### Parameters

• **text**: `string`

#### Returns

`Promise`\<`string`\>

#### Source

captcha.service.ts:106

***

### text()

> **text**(`options`?): `Promise`\<`Object`\>

#### Parameters

• **options?**: `TextCaptchaOptions`

#### Returns

`Promise`\<`Object`\>

> ##### id
>
> > **id**: `string`
>
> ##### text
>
> > **text**: `string`
>

#### Source

captcha.service.ts:75
