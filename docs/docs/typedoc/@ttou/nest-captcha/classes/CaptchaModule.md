[Documentation](../../../index.md) / [@ttou/nest-captcha](../index.md) / CaptchaModule

# Class: CaptchaModule

## Constructors

### new CaptchaModule(undefined)

> **new CaptchaModule**(): [`CaptchaModule`](CaptchaModule.md)

#### Returns

[`CaptchaModule`](CaptchaModule.md)

## Methods

### registerAsync()

> **`static`** **registerAsync**(`params`): `DynamicModule`

#### Parameters

• **params**: [`CaptchaModuleOptions`](../interfaces/CaptchaModuleOptions.md)

#### Returns

`DynamicModule`

#### Source

captcha.module.ts:12
