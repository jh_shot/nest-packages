[Documentation](../../../index.md) / [@ttou/nest-captcha](../index.md) / CaptchaModuleOptions

# Interface: CaptchaModuleOptions

## Properties

### global?

> **`optional`** **global**: `boolean`

#### Source

captcha.interfaces.ts:60

***

### optionsProvider

> **optionsProvider**: `OptionsProvider`

#### Source

captcha.interfaces.ts:59
