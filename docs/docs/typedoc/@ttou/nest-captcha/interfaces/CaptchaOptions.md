[Documentation](../../../index.md) / [@ttou/nest-captcha](../index.md) / CaptchaOptions

# Interface: CaptchaOptions

## Extends

- `BaseCaptchaOptions`

## Properties

### default?

> **`optional`** **default**: `BaseCaptchaOptions`

#### Source

captcha.interfaces.ts:28

***

### expirationTime?

> **`optional`** **expirationTime**: `number`

验证码过期时间，默认为 1h

#### Source

captcha.interfaces.ts:35

***

### formula?

> **`optional`** **formula**: `FormulaCaptchaOptions`

#### Source

captcha.interfaces.ts:30

***

### height?

> **`optional`** **height**: `number`

高度

#### Inherited from

`BaseCaptchaOptions.height`

#### Source

captcha.interfaces.ts:24

***

### idPrefix?

> **`optional`** **idPrefix**: `string`

验证码 key 前缀

#### Source

captcha.interfaces.ts:39

***

### image?

> **`optional`** **image**: `ImageCaptchaOptions`

#### Source

captcha.interfaces.ts:29

***

### noise?

> **`optional`** **noise**: `number`

干扰线条的数量，默认1

#### Inherited from

`BaseCaptchaOptions.noise`

#### Source

captcha.interfaces.ts:16

***

### size?

> **`optional`** **size**: `number`

验证码长度，默认4

#### Inherited from

`BaseCaptchaOptions.size`

#### Source

captcha.interfaces.ts:12

***

### text?

> **`optional`** **text**: `TextCaptchaOptions`

#### Source

captcha.interfaces.ts:31

***

### width?

> **`optional`** **width**: `number`

宽度

#### Inherited from

`BaseCaptchaOptions.width`

#### Source

captcha.interfaces.ts:20
