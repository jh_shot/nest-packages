[Documentation](../../../index.md) / [@ttou/nest-captcha](../index.md) / CAPTCHA\_SERVICE

# Variable: CAPTCHA\_SERVICE

> **`const`** **CAPTCHA\_SERVICE**: *typeof* [`CAPTCHA_SERVICE`](CAPTCHA_SERVICE.md)

验证码服务

## Source

captcha.constants.ts:6
