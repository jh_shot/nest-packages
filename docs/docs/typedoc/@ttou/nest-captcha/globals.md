[Documentation](../../index.md) / @ttou/nest-captcha

# @ttou/nest-captcha

## Classes

- [CaptchaModule](classes/CaptchaModule.md)
- [CaptchaService](classes/CaptchaService.md)

## Interfaces

- [CaptchaModuleOptions](interfaces/CaptchaModuleOptions.md)
- [CaptchaOptions](interfaces/CaptchaOptions.md)

## Variables

- [CAPTCHA\_SERVICE](variables/CAPTCHA_SERVICE.md)
