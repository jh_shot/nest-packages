[Documentation](../../../index.md) / [@ttou/nest-validators](../index.md) / IsEqualWith

# Function: IsEqualWith()

> **IsEqualWith**(`property`, `validationOptions`?): (`object`, `propertyName`) => `void`

DTO 兄弟属性相等比较

## Parameters

• **property**: `string`

• **validationOptions?**: `ValidationOptions`

## Returns

`Function`

> ### Parameters
>
> • **object**: `unknown`
>
> • **propertyName**: `string`
>
> ### Returns
>
> `void`
>

## Source

is-equal-with.ts:11
