[Documentation](../../../index.md) / [@ttou/nest-validators](../index.md) / IsNotEmptyWith

# Function: IsNotEmptyWith()

> **IsNotEmptyWith**(`property`, `validationOptions`?): (`object`, `propertyName`) => `void`

DTO 兄弟属性非空比较

## Parameters

• **property**: `string`

• **validationOptions?**: `ValidationOptions`

## Returns

`Function`

> ### Parameters
>
> • **object**: `unknown`
>
> • **propertyName**: `string`
>
> ### Returns
>
> `void`
>

## Source

is-not-empty-with.ts:11
