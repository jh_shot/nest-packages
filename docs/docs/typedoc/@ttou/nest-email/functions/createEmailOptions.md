[Documentation](../../../index.md) / [@ttou/nest-email](../index.md) / createEmailOptions

# Function: createEmailOptions()

> **createEmailOptions**(`optionsToken`, `provider`): `Provider`

## Parameters

• **optionsToken**: `InjectionToken`

• **provider**: `OptionsProvider`

## Returns

`Provider`

## Source

email.providers.ts:19
