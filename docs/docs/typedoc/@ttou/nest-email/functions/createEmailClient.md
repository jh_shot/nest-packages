[Documentation](../../../index.md) / [@ttou/nest-email](../index.md) / createEmailClient

# Function: createEmailClient()

> **createEmailClient**(`clientToken`, `optionsToken`): `Provider`

## Parameters

• **clientToken**: `InjectionToken`

• **optionsToken**: `InjectionToken`

## Returns

`Provider`

## Source

email.providers.ts:6
