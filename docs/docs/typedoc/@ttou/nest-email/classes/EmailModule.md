[Documentation](../../../index.md) / [@ttou/nest-email](../index.md) / EmailModule

# Class: EmailModule

## Constructors

### new EmailModule(undefined)

> **new EmailModule**(): [`EmailModule`](EmailModule.md)

#### Returns

[`EmailModule`](EmailModule.md)

## Methods

### registerAsync()

> **`static`** **registerAsync**(`params`): `DynamicModule`

#### Parameters

• **params**: [`EmailModuleOptions`](../interfaces/EmailModuleOptions.md)

#### Returns

`DynamicModule`

#### Source

email.module.ts:7
