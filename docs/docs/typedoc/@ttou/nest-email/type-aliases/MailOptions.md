[Documentation](../../../index.md) / [@ttou/nest-email](../index.md) / MailOptions

# Type alias: MailOptions

> **MailOptions**: `Mail.Options`

## Source

email.interfaces.ts:26
