[Documentation](../../../index.md) / [@ttou/nest-email](../index.md) / EmailClientOptions

# Type alias: EmailClientOptions

> **EmailClientOptions**: `SMTPTransport` \| `SMTPTransport.Options` \| `string` \| `SMTPPool` \| `SMTPPool.Options` \| `SendmailTransport` \| `SendmailTransport.Options` \| `StreamTransport` \| `StreamTransport.Options` \| `JSONTransport` \| `JSONTransport.Options` \| `SESTransport` \| `SESTransport.Options`

## Source

email.interfaces.ts:18
