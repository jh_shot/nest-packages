[Documentation](../../index.md) / @ttou/nest-email

# @ttou/nest-email

## Classes

- [EmailModule](classes/EmailModule.md)

## Interfaces

- [EmailModuleOptions](interfaces/EmailModuleOptions.md)

## Type Aliases

- [EmailClientOptions](type-aliases/EmailClientOptions.md)
- [MailOptions](type-aliases/MailOptions.md)

## Functions

- [createEmailClient](functions/createEmailClient.md)
- [createEmailOptions](functions/createEmailOptions.md)
