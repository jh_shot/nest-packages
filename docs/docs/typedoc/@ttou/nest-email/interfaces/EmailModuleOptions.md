[Documentation](../../../index.md) / [@ttou/nest-email](../index.md) / EmailModuleOptions

# Interface: EmailModuleOptions

## Properties

### clientToken

> **clientToken**: `InjectionToken`

#### Source

email.interfaces.ts:34

***

### global?

> **`optional`** **global**: `boolean`

#### Source

email.interfaces.ts:37

***

### optionsProvider

> **optionsProvider**: `OptionsProvider`

#### Source

email.interfaces.ts:36

***

### optionsToken

> **optionsToken**: `InjectionToken`

#### Source

email.interfaces.ts:35
