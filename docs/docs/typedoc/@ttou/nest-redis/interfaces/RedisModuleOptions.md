[Documentation](../../../index.md) / [@ttou/nest-redis](../index.md) / RedisModuleOptions

# Interface: RedisModuleOptions

## Properties

### clientToken

> **clientToken**: `InjectionToken`

#### Source

redis.interfaces.ts:35

***

### createType

> **createType**: `CreateType`

#### Source

redis.interfaces.ts:34

***

### global?

> **`optional`** **global**: `boolean`

#### Source

redis.interfaces.ts:38

***

### optionsProvider

> **optionsProvider**: `OptionsProvider`

#### Source

redis.interfaces.ts:37

***

### optionsToken

> **optionsToken**: `InjectionToken`

#### Source

redis.interfaces.ts:36
