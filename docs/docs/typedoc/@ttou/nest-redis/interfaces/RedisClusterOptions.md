[Documentation](../../../index.md) / [@ttou/nest-redis](../index.md) / RedisClusterOptions

# Interface: RedisClusterOptions

## Properties

### nodes

> **nodes**: `ClusterNode`[]

#### Source

redis.interfaces.ts:27

***

### options

> **options**: `ClusterOptions`

#### Source

redis.interfaces.ts:28
