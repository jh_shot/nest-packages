[Documentation](../../../index.md) / [@ttou/nest-redis](../index.md) / createRedisCluster

# Function: createRedisCluster()

> **createRedisCluster**(`clusterToken`, `optionsToken`): `Provider`

## Parameters

• **clusterToken**: `InjectionToken`

• **optionsToken**: `InjectionToken`

## Returns

`Provider`

## Source

redis.providers.ts:25
