[Documentation](../../../index.md) / [@ttou/nest-redis](../index.md) / createFactory

# Function: createFactory()

> **createFactory**(`type`): (`clientToken`, `optionsToken`) => `Provider` \| (`clusterToken`, `optionsToken`) => `Provider`

## Parameters

• **type**: `CreateType`

## Returns

(`clientToken`, `optionsToken`) => `Provider` \| (`clusterToken`, `optionsToken`) => `Provider`

## Source

redis.providers.ts:48
