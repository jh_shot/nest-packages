[Documentation](../../../index.md) / [@ttou/nest-redis](../index.md) / createRedisClient

# Function: createRedisClient()

> **createRedisClient**(`clientToken`, `optionsToken`): `Provider`

## Parameters

• **clientToken**: `InjectionToken`

• **optionsToken**: `InjectionToken`

## Returns

`Provider`

## Source

redis.providers.ts:12
