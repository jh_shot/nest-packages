[Documentation](../../../index.md) / [@ttou/nest-redis](../index.md) / createRedisCommonOptions

# Function: createRedisCommonOptions()

> **createRedisCommonOptions**(`optionsToken`, `provider`): `Provider`

## Parameters

• **optionsToken**: `InjectionToken`

• **provider**: `OptionsProvider`

## Returns

`Provider`

## Source

redis.providers.ts:38
