[Documentation](../../index.md) / @ttou/nest-redis

# @ttou/nest-redis

## Classes

- [RedisModule](classes/RedisModule.md)

## Interfaces

- [RedisClusterOptions](interfaces/RedisClusterOptions.md)
- [RedisModuleOptions](interfaces/RedisModuleOptions.md)

## Functions

- [createFactory](functions/createFactory.md)
- [createRedisClient](functions/createRedisClient.md)
- [createRedisCluster](functions/createRedisCluster.md)
- [createRedisCommonOptions](functions/createRedisCommonOptions.md)
