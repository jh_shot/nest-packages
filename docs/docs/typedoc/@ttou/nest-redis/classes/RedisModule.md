[Documentation](../../../index.md) / [@ttou/nest-redis](../index.md) / RedisModule

# Class: RedisModule

## Constructors

### new RedisModule(undefined)

> **new RedisModule**(): [`RedisModule`](RedisModule.md)

#### Returns

[`RedisModule`](RedisModule.md)

## Methods

### registerAsync()

> **`static`** **registerAsync**(`params`): `DynamicModule`

#### Parameters

• **params**: [`RedisModuleOptions`](../interfaces/RedisModuleOptions.md)

#### Returns

`DynamicModule`

#### Source

redis.module.ts:11
