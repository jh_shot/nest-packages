[Documentation](../../index.md) / @ttou/nest-cos

# @ttou/nest-cos

## Classes

- [COSModule](classes/COSModule.md)

## Interfaces

- [COSModuleOptions](interfaces/COSModuleOptions.md)

## Functions

- [createCOSClient](functions/createCOSClient.md)
- [createCOSOptions](functions/createCOSOptions.md)
