[Documentation](../../../index.md) / [@ttou/nest-cos](../index.md) / COSModuleOptions

# Interface: COSModuleOptions

## Properties

### clientToken

> **clientToken**: `InjectionToken`

#### Source

cos.interfaces.ts:17

***

### global?

> **`optional`** **global**: `boolean`

#### Source

cos.interfaces.ts:20

***

### optionsProvider

> **optionsProvider**: `OptionsProvider`

#### Source

cos.interfaces.ts:19

***

### optionsToken

> **optionsToken**: `InjectionToken`

#### Source

cos.interfaces.ts:18
