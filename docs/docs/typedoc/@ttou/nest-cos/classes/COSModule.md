[Documentation](../../../index.md) / [@ttou/nest-cos](../index.md) / COSModule

# Class: COSModule

## Constructors

### new COSModule(undefined)

> **new COSModule**(): [`COSModule`](COSModule.md)

#### Returns

[`COSModule`](COSModule.md)

## Methods

### registerAsync()

> **`static`** **registerAsync**(`params`): `DynamicModule`

#### Parameters

• **params**: [`COSModuleOptions`](../interfaces/COSModuleOptions.md)

#### Returns

`DynamicModule`

#### Source

cos.module.ts:7
