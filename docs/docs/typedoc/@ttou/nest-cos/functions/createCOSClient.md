[Documentation](../../../index.md) / [@ttou/nest-cos](../index.md) / createCOSClient

# Function: createCOSClient()

> **createCOSClient**(`clientToken`, `optionsToken`): `Provider`

## Parameters

• **clientToken**: `InjectionToken`

• **optionsToken**: `InjectionToken`

## Returns

`Provider`

## Source

cos.providers.ts:6
