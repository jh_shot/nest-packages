[Documentation](../../../index.md) / [@ttou/nest-cos](../index.md) / createCOSOptions

# Function: createCOSOptions()

> **createCOSOptions**(`optionsToken`, `provider`): `Provider`

## Parameters

• **optionsToken**: `InjectionToken`

• **provider**: `OptionsProvider`

## Returns

`Provider`

## Source

cos.providers.ts:19
