[Documentation](../../index.md) / @ttou/nest-oss

# @ttou/nest-oss

## Classes

- [OSSModule](classes/OSSModule.md)

## Interfaces

- [OSSModuleOptions](interfaces/OSSModuleOptions.md)

## Functions

- [createFactory](functions/createFactory.md)
- [createOSSClient](functions/createOSSClient.md)
- [createOSSCluster](functions/createOSSCluster.md)
- [createOSSCommonOptions](functions/createOSSCommonOptions.md)
- [createOSSSTS](functions/createOSSSTS.md)
