[Documentation](../../../index.md) / [@ttou/nest-oss](../index.md) / createOSSSTS

# Function: createOSSSTS()

> **createOSSSTS**(`clientToken`, `optionsToken`): `Provider`

## Parameters

• **clientToken**: `InjectionToken`

• **optionsToken**: `InjectionToken`

## Returns

`Provider`

## Source

oss.providers.ts:27
