[Documentation](../../../index.md) / [@ttou/nest-oss](../index.md) / createOSSCommonOptions

# Function: createOSSCommonOptions()

> **createOSSCommonOptions**(`optionsToken`, `provider`): `Provider`

## Parameters

• **optionsToken**: `InjectionToken`

• **provider**: `OptionsProvider`

## Returns

`Provider`

## Source

oss.providers.ts:53
