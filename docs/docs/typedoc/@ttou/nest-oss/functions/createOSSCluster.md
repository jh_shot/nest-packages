[Documentation](../../../index.md) / [@ttou/nest-oss](../index.md) / createOSSCluster

# Function: createOSSCluster()

> **createOSSCluster**(`clientToken`, `optionsToken`): `Provider`

## Parameters

• **clientToken**: `InjectionToken`

• **optionsToken**: `InjectionToken`

## Returns

`Provider`

## Source

oss.providers.ts:40
