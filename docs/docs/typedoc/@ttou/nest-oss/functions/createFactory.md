[Documentation](../../../index.md) / [@ttou/nest-oss](../index.md) / createFactory

# Function: createFactory()

> **createFactory**(`type`): (`clientToken`, `optionsToken`) => `Provider` \| (`clientToken`, `optionsToken`) => `Provider` \| (`clientToken`, `optionsToken`) => `Provider`

## Parameters

• **type**: `CreateTye`

## Returns

(`clientToken`, `optionsToken`) => `Provider` \| (`clientToken`, `optionsToken`) => `Provider` \| (`clientToken`, `optionsToken`) => `Provider`

## Source

oss.providers.ts:63
