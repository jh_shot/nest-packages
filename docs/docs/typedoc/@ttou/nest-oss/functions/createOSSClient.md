[Documentation](../../../index.md) / [@ttou/nest-oss](../index.md) / createOSSClient

# Function: createOSSClient()

> **createOSSClient**(`clientToken`, `optionsToken`): `Provider`

## Parameters

• **clientToken**: `InjectionToken`

• **optionsToken**: `InjectionToken`

## Returns

`Provider`

## Source

oss.providers.ts:14
