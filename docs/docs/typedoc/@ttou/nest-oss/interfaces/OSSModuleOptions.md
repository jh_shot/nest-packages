[Documentation](../../../index.md) / [@ttou/nest-oss](../index.md) / OSSModuleOptions

# Interface: OSSModuleOptions

## Properties

### clientToken

> **clientToken**: `InjectionToken`

#### Source

oss.interfaces.ts:33

***

### createType

> **createType**: `CreateTye`

#### Source

oss.interfaces.ts:32

***

### global?

> **`optional`** **global**: `boolean`

#### Source

oss.interfaces.ts:36

***

### optionsProvider

> **optionsProvider**: `OptionsProvider`

#### Source

oss.interfaces.ts:35

***

### optionsToken

> **optionsToken**: `InjectionToken`

#### Source

oss.interfaces.ts:34
