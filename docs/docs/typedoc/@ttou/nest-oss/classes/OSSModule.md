[Documentation](../../../index.md) / [@ttou/nest-oss](../index.md) / OSSModule

# Class: OSSModule

## Constructors

### new OSSModule(undefined)

> **new OSSModule**(): [`OSSModule`](OSSModule.md)

#### Returns

[`OSSModule`](OSSModule.md)

## Methods

### registerAsync()

> **`static`** **registerAsync**(`params`): `DynamicModule`

#### Parameters

• **params**: [`OSSModuleOptions`](../interfaces/OSSModuleOptions.md)

#### Returns

`DynamicModule`

#### Source

oss.module.ts:7
