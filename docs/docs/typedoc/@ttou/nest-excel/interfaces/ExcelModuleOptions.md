[Documentation](../../../index.md) / [@ttou/nest-excel](../index.md) / ExcelModuleOptions

# Interface: ExcelModuleOptions

Excel 模块配置

## Properties

### global?

> **`optional`** **global**: `boolean`

是否注册为全局模块

#### Source

excel.interfaces.ts:85
