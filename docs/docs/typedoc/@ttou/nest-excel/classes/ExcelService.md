[Documentation](../../../index.md) / [@ttou/nest-excel](../index.md) / ExcelService

# Class: ExcelService

## Constructors

### new ExcelService(undefined)

> **new ExcelService**(): [`ExcelService`](ExcelService.md)

#### Returns

[`ExcelService`](ExcelService.md)

## Methods

### getColumns()

> **`private`** **getColumns**(`Cls`): `any`[]

获取表格列

#### Parameters

• **Cls**: `any`

序列化类

#### Returns

`any`[]

#### Source

excel.service.ts:107

***

### getHeaders()

> **`private`** **getHeaders**(`filename`?): `Object`

获取响应头

#### Parameters

• **filename?**: `string`

文件名

#### Returns

`Object`

##### content-disposition

> **content-disposition**: `string`

##### content-type

> **content-type**: `string` = `'application/vnd.openxmlformats;charset=utf-8'`

#### Source

excel.service.ts:145

***

### getRows()

> **`private`** **getRows**(`Cls`, `data`): `Record`\<`string`, `any`\>[]

获取表格行

#### Parameters

• **Cls**: `any`

序列化类

• **data**: `any`[]

数据

#### Returns

`Record`\<`string`, `any`\>[]

#### Source

excel.service.ts:134

***

### handleCreate()

> **handleCreate**(`options`): `Promise`\<`Object`\>

处理生成

#### Parameters

• **options**: `ExcelCreateOptions`

生成参数

#### Returns

`Promise`\<`Object`\>

> ##### body
>
> > **body**: `Buffer` = `buffer`
>
> ##### headers
>
> > **headers**: `Object`
>
> ##### headers.content-disposition
>
> > **content-disposition**: `string`
>
> ##### headers.content-type
>
> > **content-type**: `string` = `'application/vnd.openxmlformats;charset=utf-8'`
>

#### Source

excel.service.ts:22

***

### handleExport()

> **handleExport**(`options`): `Promise`\<`Object`\>

处理导出

#### Parameters

• **options**: `ExcelExportOptions`

导出参数

#### Returns

`Promise`\<`Object`\>

> ##### body
>
> > **body**: `Buffer` = `buffer`
>
> ##### headers
>
> > **headers**: `Object`
>
> ##### headers.content-disposition
>
> > **content-disposition**: `string`
>
> ##### headers.content-type
>
> > **content-type**: `string` = `'application/vnd.openxmlformats;charset=utf-8'`
>

#### Source

excel.service.ts:44

***

### handleImport()

> **handleImport**(`options`): `Promise`\<`any`[]\>

处理导入

#### Parameters

• **options**: `ExcelImportOptions`

导入参数

#### Returns

`Promise`\<`any`[]\>

#### Source

excel.service.ts:68
