[Documentation](../../../index.md) / [@ttou/nest-excel](../index.md) / ExcelModule

# Class: ExcelModule

## Constructors

### new ExcelModule(undefined)

> **new ExcelModule**(): [`ExcelModule`](ExcelModule.md)

#### Returns

[`ExcelModule`](ExcelModule.md)

## Methods

### registerAsync()

> **`static`** **registerAsync**(`params`): `DynamicModule`

#### Parameters

• **params**: [`ExcelModuleOptions`](../interfaces/ExcelModuleOptions.md)

#### Returns

`DynamicModule`

#### Source

excel.module.ts:8
