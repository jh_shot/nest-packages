[Documentation](../../index.md) / @ttou/nest-excel

# @ttou/nest-excel

## Classes

- [ExcelModule](classes/ExcelModule.md)
- [ExcelService](classes/ExcelService.md)

## Interfaces

- [ExcelModuleOptions](interfaces/ExcelModuleOptions.md)

## Variables

- [EXCEL\_SERVICE](variables/EXCEL_SERVICE.md)

## Functions

- [ExcelColumn](functions/ExcelColumn.md)
