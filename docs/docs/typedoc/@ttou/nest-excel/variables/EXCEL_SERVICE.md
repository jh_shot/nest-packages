[Documentation](../../../index.md) / [@ttou/nest-excel](../index.md) / EXCEL\_SERVICE

# Variable: EXCEL\_SERVICE

> **`const`** **EXCEL\_SERVICE**: *typeof* [`EXCEL_SERVICE`](EXCEL_SERVICE.md)

表格服务

## Source

excel.constants.ts:4
