[Documentation](../../../index.md) / [@ttou/nest-excel](../index.md) / ExcelColumn

# Function: ExcelColumn()

> **ExcelColumn**(`options`?): `PropertyDecorator`

表格列装饰器

## Parameters

• **options?**: `Partial`\<`Column`\>

## Returns

`PropertyDecorator`

## Source

excel.decorators.ts:9
