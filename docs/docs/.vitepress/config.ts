import { defineConfig } from 'vitepress'
import typedocSidebar from '../typedoc/typedoc-sidebar.json'

export default defineConfig({
  title: 'NestJS 工具包集合',
  description: 'A VitePress Site',
  base: '/nest-packages/',
  outDir: '../deploy',
  lastUpdated: true,
  head: [
    [
      'link',
      {
        rel: 'icon',
        type: 'image/svg+xml',
        href: '/nest-packages/logo.svg'
      }
    ]
  ],
  themeConfig: {
    nav: [{ text: '文档', link: '/typedoc/' }],
    sidebar: [
      {
        text: '文档',
        items: typedocSidebar
      }
    ],
    socialLinks: [
      { icon: 'github', link: 'https://gitee.com/jh_shot/nest-packages' }
    ],
    search: {
      provider: 'local',
      options: {
        translations: {
          button: {
            buttonText: '查找'
          },
          modal: {
            noResultsText: '没有找到',
            footer: {
              navigateText: '导航',
              selectText: '选择',
              closeText: '关闭'
            }
          }
        }
      }
    },
    docFooter: {
      prev: '上一页',
      next: '下一页'
    },
    outlineTitle: '当前页',
    lastUpdatedText: '最近更新'
  },
  markdown: {
    theme: {
      light: 'material-theme-lighter',
      dark: 'material-theme-darker'
    }
  }
})
