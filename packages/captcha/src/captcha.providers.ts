import { Provider } from '@nestjs/common'

import {
  CAPTCHA_MERGED_OPTIONS,
  CAPTCHA_OPTIONS,
  defaultOptions
} from './captcha.constants'
import { CaptchaOptions, OptionsProvider } from './captcha.interfaces'

export function createCaptchaMergedOptions(): Provider {
  return {
    provide: CAPTCHA_MERGED_OPTIONS,
    useFactory: (options: CaptchaOptions) => {
      return Object.assign({}, defaultOptions, options || {})
    },
    inject: [CAPTCHA_OPTIONS]
  }
}

export function createCaptchaOptions(provider: OptionsProvider): Provider {
  return {
    provide: CAPTCHA_OPTIONS,
    ...provider
  }
}
