import { Provider } from '@nestjs/common';
import { OptionsProvider } from './captcha.interfaces';
export declare function createCaptchaMergedOptions(): Provider;
export declare function createCaptchaOptions(provider: OptionsProvider): Provider;
