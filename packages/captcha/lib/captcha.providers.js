"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createCaptchaOptions = exports.createCaptchaMergedOptions = void 0;
const captcha_constants_1 = require("./captcha.constants");
function createCaptchaMergedOptions() {
    return {
        provide: captcha_constants_1.CAPTCHA_MERGED_OPTIONS,
        useFactory: (options) => {
            return Object.assign({}, captcha_constants_1.defaultOptions, options || {});
        },
        inject: [captcha_constants_1.CAPTCHA_OPTIONS]
    };
}
exports.createCaptchaMergedOptions = createCaptchaMergedOptions;
function createCaptchaOptions(provider) {
    return {
        provide: captcha_constants_1.CAPTCHA_OPTIONS,
        ...provider
    };
}
exports.createCaptchaOptions = createCaptchaOptions;
