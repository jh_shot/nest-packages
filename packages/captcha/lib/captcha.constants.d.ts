import { CaptchaOptions } from './captcha.interfaces';
/**
 * 验证码服务
 */
export declare const CAPTCHA_SERVICE: unique symbol;
/**
 * 验证码配置
 */
export declare const CAPTCHA_OPTIONS: unique symbol;
/**
 * 验证码合并后配置
 */
export declare const CAPTCHA_MERGED_OPTIONS: unique symbol;
/**
 * 数字
 */
export declare const numbers = "0123456789";
/**
 * 字母
 */
export declare const letters: string;
/**
 * 默认配置
 */
export declare const defaultOptions: CaptchaOptions;
