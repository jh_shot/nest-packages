import { DynamicModule } from '@nestjs/common';
import { CaptchaModuleOptions } from './captcha.interfaces';
export declare class CaptchaModule {
    static registerAsync(params: CaptchaModuleOptions): DynamicModule;
}
