import { Cache } from 'cache-manager';
import { CaptchaOptions, FormulaCaptchaOptions, ImageCaptchaOptions, TextCaptchaOptions } from './captcha.interfaces';
export declare class CaptchaService {
    private cacheManager;
    private options;
    constructor(cacheManager: Cache, options: CaptchaOptions);
    image(options?: ImageCaptchaOptions): Promise<{
        id: string;
        imageBase64: string;
    }>;
    formula(options?: FormulaCaptchaOptions): Promise<{
        id: string;
        imageBase64: string;
    }>;
    text(options?: TextCaptchaOptions): Promise<{
        id: string;
        text: string;
    }>;
    set(text: string): Promise<string>;
    check(id: string, value: string): Promise<boolean>;
    private getStoreId;
}
