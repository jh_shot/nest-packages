"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var CaptchaCoreModule_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.CaptchaModule = void 0;
const common_1 = require("@nestjs/common");
const captcha_constants_1 = require("./captcha.constants");
const captcha_providers_1 = require("./captcha.providers");
const captcha_service_1 = require("./captcha.service");
class CaptchaModule {
    static registerAsync(params) {
        const { global, optionsProvider } = params;
        let CaptchaCoreModule = CaptchaCoreModule_1 = class CaptchaCoreModule {
            static create() {
                return {
                    global,
                    module: CaptchaCoreModule_1,
                    providers: [
                        (0, captcha_providers_1.createCaptchaOptions)(optionsProvider),
                        (0, captcha_providers_1.createCaptchaMergedOptions)(),
                        {
                            provide: captcha_constants_1.CAPTCHA_SERVICE,
                            useClass: captcha_service_1.CaptchaService
                        }
                    ],
                    exports: [captcha_constants_1.CAPTCHA_SERVICE]
                };
            }
        };
        CaptchaCoreModule = CaptchaCoreModule_1 = __decorate([
            (0, common_1.Module)({})
        ], CaptchaCoreModule);
        return CaptchaCoreModule.create();
    }
}
exports.CaptchaModule = CaptchaModule;
