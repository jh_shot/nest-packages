"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CaptchaService = void 0;
const cache_manager_1 = require("@nestjs/cache-manager");
const common_1 = require("@nestjs/common");
const mini_svg_data_uri_1 = __importDefault(require("mini-svg-data-uri"));
const nanoid_1 = require("nanoid");
const svg_captcha_1 = __importDefault(require("svg-captcha"));
const captcha_constants_1 = require("./captcha.constants");
let CaptchaService = class CaptchaService {
    constructor(cacheManager, options) {
        this.cacheManager = cacheManager;
        this.options = options;
    }
    async image(options) {
        const { width, height, type, size, noise } = Object.assign({}, this.options, this.options.default, this.options.image, options);
        let ignoreChars = '';
        switch (type) {
            case 'letter':
                ignoreChars = captcha_constants_1.numbers;
                break;
            case 'number':
                ignoreChars = captcha_constants_1.letters;
                break;
        }
        const { data, text } = svg_captcha_1.default.create({
            ignoreChars,
            width,
            height,
            size,
            noise
        });
        const id = await this.set(text);
        const imageBase64 = (0, mini_svg_data_uri_1.default)(data);
        return { id, imageBase64 };
    }
    async formula(options) {
        const { width, height, noise } = Object.assign({}, this.options, this.options.default, this.options.formula, options);
        const { data, text } = svg_captcha_1.default.createMathExpr({
            width,
            height,
            noise
        });
        const id = await this.set(text);
        const imageBase64 = (0, mini_svg_data_uri_1.default)(data);
        return { id, imageBase64 };
    }
    async text(options) {
        const textOptions = Object.assign({}, this.options, this.options.default, this.options.text, options);
        let chars = '';
        switch (textOptions.type) {
            case 'letter':
                chars = captcha_constants_1.letters;
                break;
            case 'number':
                chars = captcha_constants_1.numbers;
                break;
            default:
                chars = captcha_constants_1.letters + captcha_constants_1.numbers;
                break;
        }
        let text = '';
        while (textOptions.size--) {
            text += chars[Math.floor(Math.random() * chars.length)];
        }
        const id = await this.set(text);
        return { id, text };
    }
    async set(text) {
        const id = (0, nanoid_1.nanoid)();
        await this.cacheManager.set(this.getStoreId(id), (text || '').toLowerCase(), this.options.expirationTime);
        return id;
    }
    async check(id, value) {
        if (!id || !value) {
            return false;
        }
        const storeId = this.getStoreId(id);
        const storedValue = await this.cacheManager.get(storeId);
        if (value.toLowerCase() !== storedValue) {
            return false;
        }
        this.cacheManager.del(storeId);
        return true;
    }
    getStoreId(id) {
        if (!this.options.idPrefix) {
            return id;
        }
        return `${this.options.idPrefix}:${id}`;
    }
};
exports.CaptchaService = CaptchaService;
exports.CaptchaService = CaptchaService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)(cache_manager_1.CACHE_MANAGER)),
    __param(1, (0, common_1.Inject)(captcha_constants_1.CAPTCHA_MERGED_OPTIONS)),
    __metadata("design:paramtypes", [Object, Object])
], CaptchaService);
