"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.defaultOptions = exports.letters = exports.numbers = exports.CAPTCHA_MERGED_OPTIONS = exports.CAPTCHA_OPTIONS = exports.CAPTCHA_SERVICE = void 0;
/**
 * 验证码服务
 */
exports.CAPTCHA_SERVICE = Symbol('CAPTCHA_SERVICE');
/**
 * 验证码配置
 */
exports.CAPTCHA_OPTIONS = Symbol('CAPTCHA_OPTIONS');
/**
 * 验证码合并后配置
 */
exports.CAPTCHA_MERGED_OPTIONS = Symbol('CAPTCHA_MERGED_OPTIONS');
/**
 * 数字
 */
exports.numbers = '0123456789';
/**
 * 小写字母
 */
const lowerCaseLetters = 'abcdefghijklmnopqrstuvwxyz';
/**
 * 字母
 */
exports.letters = lowerCaseLetters + lowerCaseLetters.toUpperCase();
/**
 * 默认配置
 */
exports.defaultOptions = {
    default: {
        size: 4,
        noise: 1,
        width: 120,
        height: 40
    },
    image: {
        type: 'mixed'
    },
    formula: {},
    text: {},
    expirationTime: 60 * 60 * 1000,
    idPrefix: 'captcha'
};
