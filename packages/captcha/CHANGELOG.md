# @ttou/nest-captcha

## 1.2.5

### Patch Changes

- 格式化

## 1.2.4

### Patch Changes

- 一些小修改

## 1.2.3

### Patch Changes

- 统一模块配置参数名

## 1.2.2

### Patch Changes

- 修复配置合并逻辑

## 1.2.1

### Patch Changes

- 修复模块导出名

## 1.2.0

### Minor Changes

- 修改模块配置格式

## 1.1.0

### Minor Changes

- 使用新语法构建模块

## 1.0.6

### Patch Changes

- 项目依赖调整

## 1.0.5

### Patch Changes

- 修复类型

## 1.0.4

### Patch Changes

- 更新文档

## 1.0.3

### Patch Changes

- 移除多余代码

## 1.0.2

### Patch Changes

- 修复配置构造器导出

## 1.0.1

### Patch Changes

- 修改配置合并逻辑

## 1.0.0

### Major Changes

- 初始化
