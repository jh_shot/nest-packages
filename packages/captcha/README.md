<h1 align="center" style="margin: 30px 0; font-weight: bold;">Nest Captcha</h1>
<h4 align="center" style="margin: 30px 0; font-weight: bold;">NestJS 验证码组件</h4>
<p align="center" style="display: flex; justify-content: center; gap: 4px;">
  <a href="https://www.npmjs.com/package/@ttou/nest-captcha">
    <img
      src="https://img.shields.io/npm/v/@ttou/nest-captcha.svg?style=flat-square"
      alt=""
    />
  </a>
  <a href="https://npm-stat.com/charts.html?package=@ttou/nest-captcha">
    <img
      src="https://img.shields.io/npm/dt/@ttou/nest-captcha.svg?style=flat-square"
      alt=""
    />
  </a>
</p>
