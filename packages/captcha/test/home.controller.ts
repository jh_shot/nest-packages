import { Controller, Get, HttpCode, Inject, Post, Req } from '@nestjs/common'
import { Request } from 'express'

import { CAPTCHA_SERVICE, CaptchaService } from '../src'

@Controller()
export class HomeController {
  constructor(
    @Inject(CAPTCHA_SERVICE)
    private readonly captchaService: CaptchaService
  ) {}

  @Post('/text')
  @HttpCode(200)
  async text(@Req() request: Request) {
    const { text } = request.body
    const id = await this.captchaService.set(text)
    return { id }
  }

  @Post('/check')
  @HttpCode(200)
  async check(@Req() request: Request) {
    const { id, code } = request.body
    const same = await this.captchaService.check(id, code)
    return { same }
  }

  @Get('/img')
  async img() {
    return this.captchaService.image()
  }

  @Get('/formula')
  async formula() {
    return this.captchaService.formula()
  }

  @Get('/text')
  async textCode() {
    return this.captchaService.text()
  }
}
