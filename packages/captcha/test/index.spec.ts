import assert from 'node:assert'
import { CacheModule } from '@nestjs/cache-manager'
import { INestApplication } from '@nestjs/common'
import { Test } from '@nestjs/testing'
import request from 'supertest'
import { afterAll, beforeAll, describe, expect, it } from 'vitest'

import {
  CAPTCHA_SERVICE,
  CaptchaModule,
  CaptchaOptions,
  CaptchaService
} from '../src'
import { HomeController } from './home.controller'

const sleep = (time: number) => {
  return new Promise((resolve) => setTimeout(resolve, time))
}

describe('CaptchaService', () => {
  let app: INestApplication
  let captchaService: CaptchaService

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        CacheModule.register({ isGlobal: true }),
        CaptchaModule.registerAsync({
          global: true,
          optionsProvider: {
            useValue: {
              expirationTime: 1500
            } as CaptchaOptions
          }
        })
      ],
      controllers: [HomeController]
    }).compile()

    // @ts-ignore
    app = moduleRef.createNestApplication()
    captchaService = moduleRef.get(CAPTCHA_SERVICE)
    await app.init()
  })

  afterAll(async () => {
    await app.close()
  })

  it('text code', async () => {
    const mockApp = request(app.getHttpServer())
    const text = `t-${Date.now()}`

    const setRes = await mockApp
      .post('/text')
      .send({ text })
      .expect(200)
      .then(async (response) => {
        return response.body
      })
    const checkRes = await mockApp
      .post('/check')
      .send({ id: setRes.id, code: text })
      .expect(200)
      .then(async (response) => {
        return response.body
      })
    assert(checkRes.same === true)

    const imgRes = await mockApp
      .get('/img')
      .expect(200)
      .then(async (response) => {
        return response.body
      })
    assert(imgRes.id && imgRes.imageBase64)

    const formulaRes = await mockApp
      .get('/formula')
      .expect(200)
      .then(async (response) => {
        return response.body
      })
    assert(formulaRes.id && formulaRes.imageBase64)

    const textCodeRes = await mockApp
      .get('/text')
      .expect(200)
      .then(async (response) => {
        return response.body
      })
    assert(textCodeRes.id && textCodeRes.text)

    const textCheckRes = await mockApp
      .post('/check')
      .send({ id: textCodeRes.id, code: textCodeRes.text })
      .expect(200)
      .then(async (response) => {
        return response.body
      })
    assert(textCheckRes.same === true)
  })

  it('test CaptchaService cache with seconds', async () => {
    const id = await captchaService.set('123')
    expect(await captchaService.check(id, '123')).toBeTruthy()
    await sleep(1500)
    expect(await captchaService.check(id, '123')).toBeFalsy()
  })
})
