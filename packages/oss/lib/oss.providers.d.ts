import { InjectionToken, Provider } from '@nestjs/common';
import { CreateTye, OptionsProvider } from './oss.interfaces';
export declare function createOSSClient(clientToken: InjectionToken, optionsToken: InjectionToken): Provider;
export declare function createOSSSTS(clientToken: InjectionToken, optionsToken: InjectionToken): Provider;
export declare function createOSSCluster(clientToken: InjectionToken, optionsToken: InjectionToken): Provider;
export declare function createOSSCommonOptions(optionsToken: InjectionToken, provider: OptionsProvider): Provider;
export declare function createFactory(type: CreateTye): typeof createOSSClient | typeof createOSSSTS | typeof createOSSCluster;
