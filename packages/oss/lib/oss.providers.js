"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createFactory = exports.createOSSCommonOptions = exports.createOSSCluster = exports.createOSSSTS = exports.createOSSClient = void 0;
const oss_interfaces_1 = require("./oss.interfaces");
function createOSSClient(clientToken, optionsToken) {
    return {
        provide: clientToken,
        useFactory: (options) => {
            return new oss_interfaces_1.OSSClient(options);
        },
        inject: [optionsToken]
    };
}
exports.createOSSClient = createOSSClient;
function createOSSSTS(clientToken, optionsToken) {
    return {
        provide: clientToken,
        useFactory: (options) => {
            return new oss_interfaces_1.OSSSTS(options);
        },
        inject: [optionsToken]
    };
}
exports.createOSSSTS = createOSSSTS;
function createOSSCluster(clientToken, optionsToken) {
    return {
        provide: clientToken,
        useFactory: (options) => {
            return new oss_interfaces_1.OSSCluster(options);
        },
        inject: [optionsToken]
    };
}
exports.createOSSCluster = createOSSCluster;
function createOSSCommonOptions(optionsToken, provider) {
    return {
        provide: optionsToken,
        ...provider
    };
}
exports.createOSSCommonOptions = createOSSCommonOptions;
function createFactory(type) {
    const factory = {
        oss: createOSSClient,
        sts: createOSSSTS,
        cluster: createOSSCluster
    };
    return factory[type];
}
exports.createFactory = createFactory;
