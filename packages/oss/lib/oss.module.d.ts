import { DynamicModule } from '@nestjs/common';
import { OSSModuleOptions } from './oss.interfaces';
export declare class OSSModule {
    static registerAsync(params: OSSModuleOptions): DynamicModule;
}
