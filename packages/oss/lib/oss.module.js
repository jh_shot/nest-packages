"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var OSSCoreModule_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.OSSModule = void 0;
const common_1 = require("@nestjs/common");
const oss_providers_1 = require("./oss.providers");
class OSSModule {
    static registerAsync(params) {
        const { global, createType, clientToken, optionsToken, optionsProvider } = params;
        let OSSCoreModule = OSSCoreModule_1 = class OSSCoreModule {
            static create() {
                return {
                    global,
                    module: OSSCoreModule_1,
                    providers: [
                        (0, oss_providers_1.createOSSCommonOptions)(optionsToken, optionsProvider),
                        (0, oss_providers_1.createFactory)(createType)(clientToken, optionsToken)
                    ],
                    exports: [clientToken]
                };
            }
        };
        OSSCoreModule = OSSCoreModule_1 = __decorate([
            (0, common_1.Module)({})
        ], OSSCoreModule);
        return OSSCoreModule.create();
    }
}
exports.OSSModule = OSSModule;
