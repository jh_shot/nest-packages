import { ClassProvider, FactoryProvider, InjectionToken, ValueProvider } from '@nestjs/common';
import OSS, { ClusterClient, ClusterOptions, Options, STS, STSOptions } from 'ali-oss';
export { OSS as OSSClient, Options as OSSClientOptions, ClusterClient as OSSCluster, ClusterOptions as OSSClusterOptions, STS as OSSSTS, STSOptions as OSSSTSOptions };
export type OptionsProvider = Omit<ValueProvider, 'provide'> | Omit<ClassProvider, 'provide'> | Omit<FactoryProvider, 'provide'>;
export type CreateTye = 'oss' | 'sts' | 'cluster';
export interface OSSModuleOptions {
    createType: CreateTye;
    clientToken: InjectionToken;
    optionsToken: InjectionToken;
    optionsProvider: OptionsProvider;
    global?: boolean;
}
