import { DynamicModule, Module } from '@nestjs/common'

import { OSSModuleOptions } from './oss.interfaces'
import { createFactory, createOSSCommonOptions } from './oss.providers'

export class OSSModule {
  static registerAsync(params: OSSModuleOptions) {
    const { global, createType, clientToken, optionsToken, optionsProvider } =
      params

    @Module({})
    class OSSCoreModule {
      static create(): DynamicModule {
        return {
          global,
          module: OSSCoreModule,
          providers: [
            createOSSCommonOptions(optionsToken, optionsProvider),
            createFactory(createType)(clientToken, optionsToken)
          ],
          exports: [clientToken]
        }
      }
    }

    return OSSCoreModule.create()
  }
}
