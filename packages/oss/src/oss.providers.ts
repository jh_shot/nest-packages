import { InjectionToken, Provider } from '@nestjs/common'

import {
  CreateTye,
  OSSClient,
  OSSClientOptions,
  OSSCluster,
  OSSClusterOptions,
  OSSSTS,
  OSSSTSOptions,
  OptionsProvider
} from './oss.interfaces'

export function createOSSClient(
  clientToken: InjectionToken,
  optionsToken: InjectionToken
): Provider {
  return {
    provide: clientToken,
    useFactory: (options: OSSClientOptions) => {
      return new OSSClient(options)
    },
    inject: [optionsToken]
  }
}

export function createOSSSTS(
  clientToken: InjectionToken,
  optionsToken: InjectionToken
): Provider {
  return {
    provide: clientToken,
    useFactory: (options: OSSSTSOptions) => {
      return new OSSSTS(options)
    },
    inject: [optionsToken]
  }
}

export function createOSSCluster(
  clientToken: InjectionToken,
  optionsToken: InjectionToken
): Provider {
  return {
    provide: clientToken,
    useFactory: (options: OSSClusterOptions) => {
      return new OSSCluster(options)
    },
    inject: [optionsToken]
  }
}

export function createOSSCommonOptions(
  optionsToken: InjectionToken,
  provider: OptionsProvider
): Provider {
  return {
    provide: optionsToken,
    ...provider
  }
}

export function createFactory(type: CreateTye) {
  const factory = {
    oss: createOSSClient,
    sts: createOSSSTS,
    cluster: createOSSCluster
  }

  return factory[type]
}
