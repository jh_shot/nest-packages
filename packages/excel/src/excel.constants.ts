/**
 * 表格服务
 */
export const EXCEL_SERVICE = Symbol('EXCEL_SERVICE')

/**
 * 表格列装饰器标识
 */
export const EXCEL_COLUMN_METADATA = 'excel_module:excel_column'

/**
 * 表格列序列化群组名
 */
export const EXCEL_COLUMN_GROUP = 'excel_column'
