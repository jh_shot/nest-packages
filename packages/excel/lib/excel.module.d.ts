import { DynamicModule } from '@nestjs/common';
import { ExcelModuleOptions } from './excel.interfaces';
export declare class ExcelModule {
    static registerAsync(params: ExcelModuleOptions): DynamicModule;
}
