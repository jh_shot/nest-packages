import ExcelJS from 'exceljs';
import { ExcelCreateOptions, ExcelExportOptions, ExcelImportOptions } from './excel.interfaces';
export { ExcelJS };
export declare class ExcelService {
    /**
     * 处理生成
     * @param {ExcelCreateOptions} options 生成参数
     */
    handleCreate(options: ExcelCreateOptions): Promise<{
        headers: {
            'content-type': string;
            'content-disposition': string;
        };
        body: ExcelJS.Buffer;
    }>;
    /**
     * 处理导出
     * @param {ExcelExportOptions} options 导出参数
     */
    handleExport(options: ExcelExportOptions): Promise<{
        headers: {
            'content-type': string;
            'content-disposition': string;
        };
        body: ExcelJS.Buffer;
    }>;
    /**
     * 处理导入
     * @param {ExcelImportOptions} options 导入参数
     */
    handleImport(options: ExcelImportOptions): Promise<any[]>;
    /**
     * 获取表格列
     * @param {any} Cls 序列化类
     * @private
     */
    private getColumns;
    /**
     * 获取表格行
     * @param {any} Cls 序列化类
     * @param {any} data 数据
     * @private
     */
    private getRows;
    /**
     * 获取响应头
     * @param {string} filename 文件名
     * @private
     */
    private getHeaders;
}
