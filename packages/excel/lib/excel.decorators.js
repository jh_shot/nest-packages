"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExcelColumn = void 0;
const class_transformer_1 = require("class-transformer");
const excel_constants_1 = require("./excel.constants");
/**
 * 表格列装饰器
 */
function ExcelColumn(options) {
    return (...args) => {
        Reflect.metadata(excel_constants_1.EXCEL_COLUMN_METADATA, options)(...args);
        (0, class_transformer_1.Expose)({ groups: [excel_constants_1.EXCEL_COLUMN_GROUP] })(...args);
    };
}
exports.ExcelColumn = ExcelColumn;
