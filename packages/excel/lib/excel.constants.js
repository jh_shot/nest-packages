"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EXCEL_COLUMN_GROUP = exports.EXCEL_COLUMN_METADATA = exports.EXCEL_SERVICE = void 0;
/**
 * 表格服务
 */
exports.EXCEL_SERVICE = Symbol('EXCEL_SERVICE');
/**
 * 表格列装饰器标识
 */
exports.EXCEL_COLUMN_METADATA = 'excel_module:excel_column';
/**
 * 表格列序列化群组名
 */
exports.EXCEL_COLUMN_GROUP = 'excel_column';
