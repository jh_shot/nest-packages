/**
 * 表格服务
 */
export declare const EXCEL_SERVICE: unique symbol;
/**
 * 表格列装饰器标识
 */
export declare const EXCEL_COLUMN_METADATA = "excel_module:excel_column";
/**
 * 表格列序列化群组名
 */
export declare const EXCEL_COLUMN_GROUP = "excel_column";
