import { ExcelColumnOptions } from './excel.interfaces';
/**
 * 表格列装饰器
 */
export declare function ExcelColumn(options?: ExcelColumnOptions): PropertyDecorator;
