"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var ExcelCoreModule_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExcelModule = void 0;
const common_1 = require("@nestjs/common");
const excel_constants_1 = require("./excel.constants");
const excel_service_1 = require("./excel.service");
class ExcelModule {
    static registerAsync(params) {
        const { global } = params;
        let ExcelCoreModule = ExcelCoreModule_1 = class ExcelCoreModule {
            static create() {
                return {
                    global,
                    module: ExcelCoreModule_1,
                    providers: [
                        {
                            provide: excel_constants_1.EXCEL_SERVICE,
                            useClass: excel_service_1.ExcelService
                        }
                    ],
                    exports: [excel_constants_1.EXCEL_SERVICE]
                };
            }
        };
        ExcelCoreModule = ExcelCoreModule_1 = __decorate([
            (0, common_1.Module)({})
        ], ExcelCoreModule);
        return ExcelCoreModule.create();
    }
}
exports.ExcelModule = ExcelModule;
