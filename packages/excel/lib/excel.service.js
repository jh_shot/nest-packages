"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExcelService = exports.ExcelJS = void 0;
const node_crypto_1 = require("node:crypto");
const common_1 = require("@nestjs/common");
const class_transformer_1 = require("class-transformer");
const exceljs_1 = __importDefault(require("exceljs"));
exports.ExcelJS = exceljs_1.default;
const excel_constants_1 = require("./excel.constants");
let ExcelService = class ExcelService {
    /**
     * 处理生成
     * @param {ExcelCreateOptions} options 生成参数
     */
    async handleCreate(options) {
        const wb = new exceljs_1.default.Workbook();
        const ws = wb.addWorksheet(options.sheetName, options.addWorksheetOptions);
        const columns = this.getColumns(options.Cls);
        ws.columns = columns;
        const buffer = await wb.xlsx.writeBuffer();
        const headers = this.getHeaders(options.fileName);
        return {
            headers,
            body: buffer
        };
    }
    /**
     * 处理导出
     * @param {ExcelExportOptions} options 导出参数
     */
    async handleExport(options) {
        const wb = new exceljs_1.default.Workbook();
        const ws = wb.addWorksheet(options.sheetName, options.addWorksheetOptions);
        const columns = this.getColumns(options.Cls);
        const rows = this.getRows(options.Cls, options.data);
        ws.columns = columns;
        ws.addRows(rows);
        const buffer = await wb.xlsx.writeBuffer();
        const headers = this.getHeaders(options.fileName);
        return {
            headers,
            body: buffer
        };
    }
    /**
     * 处理导入
     * @param {ExcelImportOptions} options 导入参数
     */
    async handleImport(options) {
        const wb = new exceljs_1.default.Workbook();
        if (options.filePath) {
            await wb.xlsx.readFile(options.filePath);
        }
        else if (options.fileData) {
            await wb.xlsx.load(options.fileData);
        }
        else {
            throw new common_1.BadRequestException('filePath or fileData must be required');
        }
        const ws = wb.getWorksheet(options.sheetName);
        const length = ws.rowCount;
        const rows = ws.getRows(2, length - 1);
        const columns = this.getColumns(options.Cls);
        ws.columns = columns;
        const arr = [];
        for (const row of rows) {
            const obj = Object.create({});
            for (const column of columns) {
                obj[column.key] = row.getCell(column.key).value;
            }
            arr.push(obj);
        }
        return arr;
    }
    /**
     * 获取表格列
     * @param {any} Cls 序列化类
     * @private
     */
    getColumns(Cls) {
        const columns = [];
        const obj = (0, class_transformer_1.instanceToPlain)(new Cls({}), { groups: [excel_constants_1.EXCEL_COLUMN_GROUP] });
        const keys = Object.keys(obj);
        for (const key of keys) {
            const result = Reflect.getMetadata(excel_constants_1.EXCEL_COLUMN_METADATA, Cls.prototype, key);
            if (result) {
                columns.push({ key, ...result });
            }
        }
        return columns;
    }
    /**
     * 获取表格行
     * @param {any} Cls 序列化类
     * @param {any} data 数据
     * @private
     */
    getRows(Cls, data) {
        return data.map((v) => (0, class_transformer_1.instanceToPlain)(new Cls(v), { groups: [excel_constants_1.EXCEL_COLUMN_GROUP] }));
    }
    /**
     * 获取响应头
     * @param {string} filename 文件名
     * @private
     */
    getHeaders(filename) {
        const name = filename ? encodeURIComponent(filename) : (0, node_crypto_1.randomUUID)();
        return {
            'content-type': 'application/vnd.openxmlformats;charset=utf-8',
            'content-disposition': `attachment; filename=${name}.xlsx`
        };
    }
};
exports.ExcelService = ExcelService;
exports.ExcelService = ExcelService = __decorate([
    (0, common_1.Injectable)()
], ExcelService);
