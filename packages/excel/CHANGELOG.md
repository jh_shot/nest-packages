# @ttou/nest-excel

## 1.5.4

### Patch Changes

- 格式化

## 1.5.3

### Patch Changes

- 一些小修改

## 1.5.2

### Patch Changes

- 统一模块配置参数名

## 1.5.1

### Patch Changes

- 修复模块导出名

## 1.5.0

### Minor Changes

- 使用新语法构建模块

## 1.4.1

### Patch Changes

- 项目依赖调整

## 1.4.0

### Minor Changes

- 升级 exceljs

## 1.3.1

### Patch Changes

- 更新文档

## 1.3.0

### Minor Changes

- 导入支持传入文件数据

## 1.2.2

### Patch Changes

- 修复配置构造器导出

## 1.2.1

### Patch Changes

- 修改配置合并逻辑

## 1.2.0

### Minor Changes

- 一些优化

## 1.1.0

### Minor Changes

- 一些优化

## 1.0.0

### Major Changes

- 初始化
