import { join } from 'node:path'

import swc from 'unplugin-swc'
import { defineConfig } from 'vitest/config'

export default defineConfig({
  test: {
    root: __dirname,
    include: ['**/*.spec.ts']
  },
  plugins: [
    swc.vite({
      configFile: join(__dirname, './.swcrc'),
      module: { type: 'es6' }
    })
  ]
})
