import { createReadStream } from 'node:fs'
import { join } from 'node:path'

import { INestApplication } from '@nestjs/common'
import { MulterModule } from '@nestjs/platform-express'
import { Test } from '@nestjs/testing'
import request from 'supertest'
import { afterAll, beforeAll, describe, expect, it } from 'vitest'

import { EXCEL_SERVICE, ExcelJS, ExcelModule, ExcelService } from '../src'
import { HomeController } from './home.controller'

describe('Excel', () => {
  let app: INestApplication
  let excelService: ExcelService

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        MulterModule.register(),
        ExcelModule.registerAsync({
          global: true
        })
      ],
      controllers: [HomeController]
    }).compile()

    // @ts-ignore
    app = moduleRef.createNestApplication()
    excelService = moduleRef.get(EXCEL_SERVICE)
    await app.init()
  })

  afterAll(async () => {
    await app.close()
  })

  it('create excel', async () => {
    const mockApp = request(app.getHttpServer())
    const mockData = {
      fileName: 'User',
      sheetName: 'CreateUser'
    }

    const createRes = await mockApp
      .post('/create')
      .send(mockData)
      .expect(200)
      .then(async (response) => {
        return response.body
      })

    expect(createRes.headers['content-disposition']).toContain(
      mockData.fileName
    )

    const wb = new ExcelJS.Workbook()

    await wb.xlsx.load(createRes.body.data)

    const row1 = wb.getWorksheet(mockData.sheetName).getRow(1)

    expect(row1.values).toEqual([
      undefined,
      '主键',
      '用户账号',
      '用户昵称',
      '邮箱',
      '手机',
      '性别',
      '状态',
      '备注'
    ])
  })

  it('export excel', async () => {
    const mockApp = request(app.getHttpServer())
    const mockData = {
      fileName: 'User',
      sheetName: 'ExportUser',
      data: [
        {
          id: '1',
          userName: 'admin',
          nickName: '管理员',
          email: '123@321.com',
          phone: '12345678901',
          sex: '1',
          status: '0',
          remark: '系统根账号',
          extraCol: '额外列'
        }
      ]
    }

    const exportRes = await mockApp
      .post('/export')
      .send(mockData)
      .expect(200)
      .then(async (response) => {
        return response.body
      })

    expect(exportRes.headers['content-disposition']).toContain(
      mockData.fileName
    )

    const wb = new ExcelJS.Workbook()

    await wb.xlsx.load(exportRes.body.data)

    const row1 = wb.getWorksheet(mockData.sheetName).getRow(1)

    expect(row1.values).toEqual([
      undefined,
      '主键',
      '用户账号',
      '用户昵称',
      '邮箱',
      '手机',
      '性别',
      '状态',
      '备注'
    ])

    const row2 = wb.getWorksheet(mockData.sheetName).getRow(2)

    expect(row2.values).toEqual([
      undefined,
      '1',
      'admin',
      '管理员',
      '123@321.com',
      '12345678901',
      '男',
      '正常',
      '系统根账号'
    ])
  })

  it('import excel', async () => {
    const mockApp = request(app.getHttpServer())
    const file = createReadStream(join(__dirname, './User.xlsx'))

    const importRes = await mockApp
      .post('/import')
      .attach('file', file)
      .expect(200)
      .then(async (response) => {
        return response.body
      })

    expect(importRes).toEqual([
      {
        id: 1,
        userName: 'admin',
        nickName: '管理员',
        email: '123@321.com',
        phone: 12345678901,
        sex: '男',
        status: '正常',
        remark: '系统根账号'
      }
    ])
  })
})
