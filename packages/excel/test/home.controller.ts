import {
  Controller,
  HttpCode,
  Inject,
  Post,
  Req,
  UploadedFile,
  UseInterceptors
} from '@nestjs/common'
import { FileInterceptor } from '@nestjs/platform-express'
import { Request } from 'express'

import { EXCEL_SERVICE, ExcelService } from '../src'
import { UserCreateCls, UserExportCls } from './home.model'

@Controller()
export class HomeController {
  constructor(
    @Inject(EXCEL_SERVICE)
    private readonly excelService: ExcelService
  ) {}

  @Post('/create')
  @HttpCode(200)
  async create(@Req() request: Request) {
    const { fileName, sheetName } = request.body

    return await this.excelService.handleCreate({
      fileName,
      sheetName,
      Cls: UserCreateCls
    })
  }

  @Post('/export')
  @HttpCode(200)
  async export(@Req() request: Request) {
    const { fileName, sheetName, data } = request.body

    return await this.excelService.handleExport({
      fileName,
      sheetName,
      Cls: UserExportCls,
      data
    })
  }

  @Post('/import')
  @HttpCode(200)
  @UseInterceptors(FileInterceptor('file'))
  async import(@UploadedFile() file: Express.Multer.File) {
    return await this.excelService.handleImport({
      sheetName: 'CreateUser',
      fileData: file.buffer,
      Cls: UserCreateCls
    })
  }
}
