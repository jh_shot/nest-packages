import { Transform } from 'class-transformer'

import { ExcelColumn } from '../src'

export class UserCreateCls {
  @ExcelColumn({ header: '主键' })
  id: string

  @ExcelColumn({ header: '用户账号' })
  userName: string

  @ExcelColumn({ header: '用户昵称' })
  nickName: string

  @ExcelColumn({ header: '邮箱' })
  email: string

  @ExcelColumn({ header: '手机' })
  phone: string

  @ExcelColumn({ header: '性别' })
  sex: string

  @ExcelColumn({ header: '状态' })
  status: string

  @ExcelColumn({ header: '备注' })
  remark: string
}

export class UserExportCls {
  @ExcelColumn({ header: '主键' })
  id: string

  @ExcelColumn({ header: '用户账号' })
  userName: string

  @ExcelColumn({ header: '用户昵称' })
  nickName: string

  @ExcelColumn({ header: '邮箱' })
  email: string

  @ExcelColumn({ header: '手机' })
  phone: string

  @ExcelColumn({ header: '性别' })
  @Transform(({ value }) => (value === '1' ? '男' : '女'))
  sex: string

  @ExcelColumn({ header: '状态' })
  @Transform(({ value }) => (value === '0' ? '正常' : '禁用'))
  status: string

  @ExcelColumn({ header: '备注' })
  remark: string

  constructor(partial: any) {
    Object.assign(this, partial)
  }
}
