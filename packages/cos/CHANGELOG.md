# @ttou/nest-cos

## 1.1.4

### Patch Changes

- 一些小修改

## 1.1.3

### Patch Changes

- 统一模块配置参数名

## 1.1.2

### Patch Changes

- 修复模块导出名

## 1.1.1

### Patch Changes

- 修复模块导出名

## 1.1.0

### Minor Changes

- 更改模块配置格式

## 1.0.1

### Patch Changes

- 完善代码

## 1.0.0

### Major Changes

- 初始化
