import { ClassProvider, FactoryProvider, InjectionToken, ValueProvider } from '@nestjs/common';
import COS, { COSOptions } from 'cos-nodejs-sdk-v5';
export { COS as COSClient, COSOptions as COSClientOptions };
export type OptionsProvider = Omit<ValueProvider, 'provide'> | Omit<ClassProvider, 'provide'> | Omit<FactoryProvider, 'provide'>;
export interface COSModuleOptions {
    clientToken: InjectionToken;
    optionsToken: InjectionToken;
    optionsProvider: OptionsProvider;
    global?: boolean;
}
