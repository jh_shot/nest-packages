import { DynamicModule } from '@nestjs/common';
import { COSModuleOptions } from './cos.interfaces';
export declare class COSModule {
    static registerAsync(params: COSModuleOptions): DynamicModule;
}
