import { InjectionToken, Provider } from '@nestjs/common';
import { OptionsProvider } from './cos.interfaces';
export declare function createCOSClient(clientToken: InjectionToken, optionsToken: InjectionToken): Provider;
export declare function createCOSOptions(optionsToken: InjectionToken, provider: OptionsProvider): Provider;
