"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createCOSOptions = exports.createCOSClient = void 0;
const cos_nodejs_sdk_v5_1 = __importDefault(require("cos-nodejs-sdk-v5"));
function createCOSClient(clientToken, optionsToken) {
    return {
        provide: clientToken,
        useFactory: (options) => {
            return new cos_nodejs_sdk_v5_1.default(options);
        },
        inject: [optionsToken]
    };
}
exports.createCOSClient = createCOSClient;
function createCOSOptions(optionsToken, provider) {
    return {
        provide: optionsToken,
        ...provider
    };
}
exports.createCOSOptions = createCOSOptions;
