"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var COSCoreModule_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.COSModule = void 0;
const common_1 = require("@nestjs/common");
const cos_providers_1 = require("./cos.providers");
class COSModule {
    static registerAsync(params) {
        const { global, clientToken, optionsToken, optionsProvider } = params;
        let COSCoreModule = COSCoreModule_1 = class COSCoreModule {
            static create() {
                return {
                    global,
                    module: COSCoreModule_1,
                    providers: [
                        (0, cos_providers_1.createCOSOptions)(optionsToken, optionsProvider),
                        (0, cos_providers_1.createCOSClient)(clientToken, optionsToken)
                    ],
                    exports: [clientToken]
                };
            }
        };
        COSCoreModule = COSCoreModule_1 = __decorate([
            (0, common_1.Module)({})
        ], COSCoreModule);
        return COSCoreModule.create();
    }
}
exports.COSModule = COSModule;
