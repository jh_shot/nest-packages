import { DynamicModule, Module } from '@nestjs/common'

import { COSModuleOptions } from './cos.interfaces'
import { createCOSClient, createCOSOptions } from './cos.providers'

export class COSModule {
  static registerAsync(params: COSModuleOptions) {
    const { global, clientToken, optionsToken, optionsProvider } = params

    @Module({})
    class COSCoreModule {
      static create(): DynamicModule {
        return {
          global,
          module: COSCoreModule,
          providers: [
            createCOSOptions(optionsToken, optionsProvider),
            createCOSClient(clientToken, optionsToken)
          ],
          exports: [clientToken]
        }
      }
    }

    return COSCoreModule.create()
  }
}
