export { COSClient, COSClientOptions, COSModuleOptions } from './cos.interfaces'
export * from './cos.module'
export * from './cos.providers'
