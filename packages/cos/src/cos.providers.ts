import { InjectionToken, Provider } from '@nestjs/common'
import COS from 'cos-nodejs-sdk-v5'

import { COSClientOptions, OptionsProvider } from './cos.interfaces'

export function createCOSClient(
  clientToken: InjectionToken,
  optionsToken: InjectionToken
): Provider {
  return {
    provide: clientToken,
    useFactory: (options: COSClientOptions) => {
      return new COS(options)
    },
    inject: [optionsToken]
  }
}

export function createCOSOptions(
  optionsToken: InjectionToken,
  provider: OptionsProvider
): Provider {
  return {
    provide: optionsToken,
    ...provider
  }
}
