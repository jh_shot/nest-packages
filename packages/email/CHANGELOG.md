# @ttou/nest-email

## 2.2.4

### Patch Changes

- 一些小修改

## 2.2.3

### Patch Changes

- 统一模块配置参数名

## 2.2.2

### Patch Changes

- 修复模块导出名

## 2.2.1

### Patch Changes

- 修复模块导出名

## 2.2.0

### Minor Changes

- 修改模块配置格式

## 2.1.1

### Patch Changes

- 完善文档
- 完善代码

## 2.1.0

### Minor Changes

- 提供创建模块方法

## 2.0.0

### Major Changes

- 使用 provider 重构模块

## 1.3.0

### Minor Changes

- 使用新语法构建模块

## 1.2.5

### Patch Changes

- 项目依赖调整

## 1.2.4

### Patch Changes

- 更新文档

## 1.2.3

### Patch Changes

- 修复配置构造器导出

## 1.2.2

### Patch Changes

- 修改配置合并逻辑

## 1.2.1

### Patch Changes

- 导出测试方法

## 1.2.0

### Minor Changes

- 使用 nodemailer 发送邮件

## 1.1.0

### Minor Changes

- 使用 emailjs 发送邮件

## 1.0.0

### Major Changes

- 初始化
