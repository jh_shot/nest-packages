import { DynamicModule, Inject, Module, OnModuleDestroy } from '@nestjs/common'

import { EmailClient, EmailModuleOptions } from './email.interfaces'
import { createEmailClient, createEmailOptions } from './email.providers'

export class EmailModule {
  static registerAsync(params: EmailModuleOptions) {
    const { global, clientToken, optionsToken, optionsProvider } = params

    @Module({})
    class EmailCoreModule implements OnModuleDestroy {
      constructor(
        @Inject(clientToken)
        private readonly client: EmailClient
      ) {}

      static create(): DynamicModule {
        return {
          global,
          module: EmailCoreModule,
          providers: [
            createEmailOptions(optionsToken, optionsProvider),
            createEmailClient(clientToken, optionsToken)
          ],
          exports: [clientToken]
        }
      }

      onModuleDestroy() {
        this.client.close()
      }
    }

    return EmailCoreModule.create()
  }
}
