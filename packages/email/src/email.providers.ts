import { InjectionToken, Provider } from '@nestjs/common'
import { createTransport } from 'nodemailer'

import { EmailClientOptions, OptionsProvider } from './email.interfaces'

export function createEmailClient(
  clientToken: InjectionToken,
  optionsToken: InjectionToken
): Provider {
  return {
    provide: clientToken,
    useFactory: (options: EmailClientOptions) => {
      return createTransport(options)
    },
    inject: [optionsToken]
  }
}

export function createEmailOptions(
  optionsToken: InjectionToken,
  provider: OptionsProvider
): Provider {
  return {
    provide: optionsToken,
    ...provider
  }
}
