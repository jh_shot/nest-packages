import { InjectionToken, Provider } from '@nestjs/common';
import { OptionsProvider } from './email.interfaces';
export declare function createEmailClient(clientToken: InjectionToken, optionsToken: InjectionToken): Provider;
export declare function createEmailOptions(optionsToken: InjectionToken, provider: OptionsProvider): Provider;
