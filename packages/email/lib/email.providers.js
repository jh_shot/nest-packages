"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createEmailOptions = exports.createEmailClient = void 0;
const nodemailer_1 = require("nodemailer");
function createEmailClient(clientToken, optionsToken) {
    return {
        provide: clientToken,
        useFactory: (options) => {
            return (0, nodemailer_1.createTransport)(options);
        },
        inject: [optionsToken]
    };
}
exports.createEmailClient = createEmailClient;
function createEmailOptions(optionsToken, provider) {
    return {
        provide: optionsToken,
        ...provider
    };
}
exports.createEmailOptions = createEmailOptions;
