export { EmailClient, EmailClientOptions, EmailModuleOptions, MailOptions } from './email.interfaces';
export * from './email.module';
export * from './email.providers';
export { getTestMessageUrl } from 'nodemailer';
