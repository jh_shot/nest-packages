import { ClassProvider, FactoryProvider, InjectionToken, ValueProvider } from '@nestjs/common';
import { Transporter } from 'nodemailer';
import type JSONTransport from 'nodemailer/lib/json-transport';
import type Mail from 'nodemailer/lib/mailer';
import type SendmailTransport from 'nodemailer/lib/sendmail-transport';
import type SESTransport from 'nodemailer/lib/ses-transport';
import type SMTPPool from 'nodemailer/lib/smtp-pool';
import type SMTPTransport from 'nodemailer/lib/smtp-transport';
import type StreamTransport from 'nodemailer/lib/stream-transport';
export { Transporter as EmailClient };
export type EmailClientOptions = (SMTPTransport | SMTPTransport.Options | string) | (SMTPPool | SMTPPool.Options) | (SendmailTransport | SendmailTransport.Options) | (StreamTransport | StreamTransport.Options) | (JSONTransport | JSONTransport.Options) | (SESTransport | SESTransport.Options);
export type MailOptions = Mail.Options;
export type OptionsProvider = Omit<ValueProvider, 'provide'> | Omit<ClassProvider, 'provide'> | Omit<FactoryProvider, 'provide'>;
export interface EmailModuleOptions {
    clientToken: InjectionToken;
    optionsToken: InjectionToken;
    optionsProvider: OptionsProvider;
    global?: boolean;
}
