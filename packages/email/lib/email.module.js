"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var EmailCoreModule_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.EmailModule = void 0;
const common_1 = require("@nestjs/common");
const email_providers_1 = require("./email.providers");
class EmailModule {
    static registerAsync(params) {
        const { global, clientToken, optionsToken, optionsProvider } = params;
        let EmailCoreModule = EmailCoreModule_1 = class EmailCoreModule {
            constructor(client) {
                this.client = client;
            }
            static create() {
                return {
                    global,
                    module: EmailCoreModule_1,
                    providers: [
                        (0, email_providers_1.createEmailOptions)(optionsToken, optionsProvider),
                        (0, email_providers_1.createEmailClient)(clientToken, optionsToken)
                    ],
                    exports: [clientToken]
                };
            }
            onModuleDestroy() {
                this.client.close();
            }
        };
        EmailCoreModule = EmailCoreModule_1 = __decorate([
            (0, common_1.Module)({}),
            __param(0, (0, common_1.Inject)(clientToken)),
            __metadata("design:paramtypes", [Object])
        ], EmailCoreModule);
        return EmailCoreModule.create();
    }
}
exports.EmailModule = EmailModule;
