import { DynamicModule } from '@nestjs/common';
import { EmailModuleOptions } from './email.interfaces';
export declare class EmailModule {
    static registerAsync(params: EmailModuleOptions): DynamicModule;
}
