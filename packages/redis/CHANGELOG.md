# @ttou/nest-redis

## 2.3.6

### Patch Changes

- 一些小修改

## 2.3.5

### Patch Changes

- 统一模块配置参数名

## 2.3.4

### Patch Changes

- 修复模块导出名

## 2.3.3

### Patch Changes

- 更新文档

## 2.3.2

### Patch Changes

- 修复模块导出名

## 2.3.1

### Patch Changes

- 修复类型导出

## 2.3.0

### Minor Changes

- 更改配置格式

## 2.2.0

### Minor Changes

- 完善文档
- 增加配置项

## 2.1.0

### Minor Changes

- 提供创建模块方法

## 2.0.1

### Patch Changes

- 修复类型

## 2.0.0

### Major Changes

- 使用 provider 重构模块

## 1.1.1

### Patch Changes

- 修复类型导出

## 1.1.0

### Minor Changes

- 使用新语法构建模块

## 1.0.3

### Patch Changes

- 项目依赖调整

## 1.0.2

### Patch Changes

- 修复类型

## 1.0.1

### Patch Changes

- 修复类型

## 1.0.0

### Major Changes

- 初始化
