import { DynamicModule } from '@nestjs/common';
import { RedisModuleOptions } from './redis.interfaces';
export declare class RedisModule {
    static registerAsync(params: RedisModuleOptions): DynamicModule;
}
