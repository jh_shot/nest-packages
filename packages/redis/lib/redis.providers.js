"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createFactory = exports.createRedisCommonOptions = exports.createRedisCluster = exports.createRedisClient = void 0;
const redis_interfaces_1 = require("./redis.interfaces");
function createRedisClient(clientToken, optionsToken) {
    return {
        provide: clientToken,
        useFactory: (options) => {
            return new redis_interfaces_1.RedisClient(options);
        },
        inject: [optionsToken]
    };
}
exports.createRedisClient = createRedisClient;
function createRedisCluster(clusterToken, optionsToken) {
    return {
        provide: clusterToken,
        useFactory: (options) => {
            return new redis_interfaces_1.RedisCluster(options.nodes, options.options);
        },
        inject: [optionsToken]
    };
}
exports.createRedisCluster = createRedisCluster;
function createRedisCommonOptions(optionsToken, provider) {
    return {
        provide: optionsToken,
        ...provider
    };
}
exports.createRedisCommonOptions = createRedisCommonOptions;
function createFactory(type) {
    const factory = {
        client: createRedisClient,
        cluster: createRedisCluster
    };
    return factory[type];
}
exports.createFactory = createFactory;
