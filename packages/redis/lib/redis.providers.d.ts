import { InjectionToken, Provider } from '@nestjs/common';
import { CreateType, OptionsProvider } from './redis.interfaces';
export declare function createRedisClient(clientToken: InjectionToken, optionsToken: InjectionToken): Provider;
export declare function createRedisCluster(clusterToken: InjectionToken, optionsToken: InjectionToken): Provider;
export declare function createRedisCommonOptions(optionsToken: InjectionToken, provider: OptionsProvider): Provider;
export declare function createFactory(type: CreateType): typeof createRedisClient | typeof createRedisCluster;
