"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RedisCluster = exports.RedisClient = void 0;
const ioredis_1 = require("ioredis");
Object.defineProperty(exports, "RedisCluster", { enumerable: true, get: function () { return ioredis_1.Cluster; } });
Object.defineProperty(exports, "RedisClient", { enumerable: true, get: function () { return ioredis_1.Redis; } });
