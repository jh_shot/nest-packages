"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var RedisCoreModule_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.RedisModule = void 0;
const common_1 = require("@nestjs/common");
const redis_providers_1 = require("./redis.providers");
class RedisModule {
    static registerAsync(params) {
        const { global, createType, clientToken, optionsToken, optionsProvider } = params;
        let RedisCoreModule = RedisCoreModule_1 = class RedisCoreModule {
            constructor(client) {
                this.client = client;
            }
            static create() {
                return {
                    module: RedisCoreModule_1,
                    global,
                    providers: [
                        (0, redis_providers_1.createRedisCommonOptions)(optionsToken, optionsProvider),
                        (0, redis_providers_1.createFactory)(createType)(clientToken, optionsToken)
                    ],
                    exports: [clientToken]
                };
            }
            async onModuleDestroy() {
                await this.client.quit();
            }
        };
        RedisCoreModule = RedisCoreModule_1 = __decorate([
            (0, common_1.Module)({}),
            __param(0, (0, common_1.Inject)(clientToken)),
            __metadata("design:paramtypes", [Object])
        ], RedisCoreModule);
        return RedisCoreModule.create();
    }
}
exports.RedisModule = RedisModule;
