import { INestApplication } from '@nestjs/common'
import { Test } from '@nestjs/testing'
import { afterEach, describe, expect, it } from 'vitest'

import { RedisClient, RedisClientOptions, RedisModule } from '../src'

describe('Redis', () => {
  let app: INestApplication
  let redisClient: RedisClient
  const REDIS_CLIENT = Symbol('REDIS_CLIENT')
  const REDIS_CLIENT_OPTIONS = Symbol('REDIS_CLIENT_OPTIONS')

  afterEach(async () => {
    await app.close()
  })

  it('test single client', async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        RedisModule.registerAsync({
          createType: 'client',
          clientToken: REDIS_CLIENT,
          optionsToken: REDIS_CLIENT_OPTIONS,
          optionsProvider: {
            useValue: {
              host: '127.0.0.1',
              port: 6379,
              db: 0
            } as RedisClientOptions
          }
        })
      ]
    }).compile()

    // @ts-ignore
    app = moduleRef.createNestApplication()
    redisClient = moduleRef.get(REDIS_CLIENT)
    await app.init()

    await redisClient.set('foo', 'bar')
    const result = await redisClient.get('foo')
    expect(result).toEqual('bar')
  })

  it('support custom command define', async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        RedisModule.registerAsync({
          createType: 'client',
          clientToken: REDIS_CLIENT,
          optionsToken: REDIS_CLIENT_OPTIONS,
          optionsProvider: {
            useValue: {
              host: '127.0.0.1',
              port: 6379,
              db: 0
            } as RedisClientOptions
          }
        })
      ]
    }).compile()

    // @ts-ignore
    app = moduleRef.createNestApplication()
    redisClient = moduleRef.get(REDIS_CLIENT)
    await app.init()

    redisClient.defineCommand('myecho', {
      numberOfKeys: 2,
      lua: 'return {KEYS[1],KEYS[2],ARGV[1],ARGV[2]}'
    })
    // @ts-ignore
    const result = await redisClient.myecho('k1', 'k2', 'a1', 'a2')
    expect(result).toEqual(['k1', 'k2', 'a1', 'a2'])
  })
})
