# @ttou/nest-validators

## 0.2.2

### Patch Changes

- 格式化

## 0.2.1

### Patch Changes

- 一些小修改

## 0.2.0

### Minor Changes

- 增加默认错误信息

## 0.1.2

### Patch Changes

- 项目依赖调整

## 0.1.1

### Patch Changes

- 更新文档

## 0.1.0

### Minor Changes

- 增加兄弟属性非空校验

## 0.0.1

### Patch Changes

- 初始化
