"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IsNotEmptyWith = void 0;
const class_validator_1 = require("class-validator");
/**
 * DTO 兄弟属性非空比较
 */
function IsNotEmptyWith(property, validationOptions) {
    return (object, propertyName) => {
        (0, class_validator_1.registerDecorator)({
            name: 'IsNotEmptyWith',
            target: object.constructor,
            propertyName: propertyName,
            constraints: [property],
            options: validationOptions,
            validator: {
                validate(value, args) {
                    const [relatedPropertyName] = args.constraints;
                    const relatedValue = args.object[relatedPropertyName];
                    if ((0, class_validator_1.isNotEmpty)(relatedValue)) {
                        return (0, class_validator_1.isNotEmpty)(value);
                    }
                    return true;
                },
                defaultMessage: (0, class_validator_1.buildMessage)((eachPrefix) => `${eachPrefix}$property should not empty with $constraint1`, validationOptions)
            }
        });
    };
}
exports.IsNotEmptyWith = IsNotEmptyWith;
