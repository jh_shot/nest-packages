import { ValidationOptions } from 'class-validator';
/**
 * DTO 兄弟属性相等比较
 */
export declare function IsEqualWith(property: string, validationOptions?: ValidationOptions): (object: unknown, propertyName: string) => void;
