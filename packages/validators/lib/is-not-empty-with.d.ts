import { ValidationOptions } from 'class-validator';
/**
 * DTO 兄弟属性非空比较
 */
export declare function IsNotEmptyWith(property: string, validationOptions?: ValidationOptions): (object: unknown, propertyName: string) => void;
