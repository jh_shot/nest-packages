"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.IsEqualWith = void 0;
const class_validator_1 = require("class-validator");
/**
 * DTO 兄弟属性相等比较
 */
function IsEqualWith(property, validationOptions) {
    return (object, propertyName) => {
        (0, class_validator_1.registerDecorator)({
            name: 'IsEqualWith',
            target: object.constructor,
            propertyName: propertyName,
            constraints: [property],
            options: validationOptions,
            validator: {
                validate(value, args) {
                    const [relatedPropertyName] = args.constraints;
                    const relatedValue = args.object[relatedPropertyName];
                    return (0, class_validator_1.equals)(value, relatedValue);
                },
                defaultMessage: (0, class_validator_1.buildMessage)((eachPrefix) => `${eachPrefix}$property should equal with $constraint1`, validationOptions)
            }
        });
    };
}
exports.IsEqualWith = IsEqualWith;
