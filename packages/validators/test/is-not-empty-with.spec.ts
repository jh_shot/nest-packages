import { Validator } from 'class-validator'
import { describe, expect, it } from 'vitest'

import { IsNotEmptyWith } from '../src'

const validator = new Validator()

describe('IsNotEmptyWith', () => {
  it('test case 1', () => {
    expect.assertions(1)

    class ModelClass {
      @IsNotEmptyWith('val2')
      val1 = '222'

      val2 = '1234'
    }

    const model = new ModelClass()

    return validator.validate(model).then((errors) => {
      expect(errors.length).toEqual(0)
    })
  })

  it('test case 2', () => {
    expect.assertions(5)

    class ModelClass {
      @IsNotEmptyWith('val2')
      val1: string

      val2 = '1234'
    }

    const model = new ModelClass()

    return validator.validate(model).then((errors) => {
      expect(errors.length).toEqual(1)
      expect(errors[0].target).toEqual(model)
      expect(errors[0].property).toEqual('val1')
      expect(errors[0].constraints).toEqual({
        IsNotEmptyWith: 'val1 should not empty with val2'
      })
      expect(errors[0].value).toEqual(undefined)
    })
  })
})
