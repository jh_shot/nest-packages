import { Validator } from 'class-validator'
import { describe, expect, it } from 'vitest'

import { IsEqualWith } from '../src'

const validator = new Validator()

describe('IsEqualWith', () => {
  it('test case 1', () => {
    expect.assertions(1)

    class ModelClass {
      @IsEqualWith('val2')
      val1: string

      val2: string

      constructor(partial: Partial<ModelClass>) {
        Object.assign(this, partial)
      }
    }

    const model = new ModelClass({
      val1: '1234',
      val2: '1234'
    })

    return validator.validate(model).then((errors) => {
      expect(errors.length).toEqual(0)
    })
  })

  it('test case 2', () => {
    expect.assertions(5)

    class ModelClass {
      @IsEqualWith('val2')
      val1: string

      val2: string

      constructor(partial: Partial<ModelClass>) {
        Object.assign(this, partial)
      }
    }

    const model = new ModelClass({
      val1: '1234',
      val2: '4321'
    })

    return validator.validate(model).then((errors) => {
      expect(errors.length).toEqual(1)
      expect(errors[0].target).toEqual(model)
      expect(errors[0].property).toEqual('val1')
      expect(errors[0].constraints).toEqual({
        IsEqualWith: 'val1 should equal with val2'
      })
      expect(errors[0].value).toEqual('1234')
    })
  })
})
